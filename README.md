# RingCentral WebPhone Library

## Installing

```sh
git clone https://github.com/ringcentral/web-phone.git
```

## Usage

### Include Library:

```html
<script type="text/javascript" src="path/to/library/dist/rc-web-phone.js"></script>    
```

### Application:


Thirst of all we need to create and configure the web-phone:
	
```javascript
//Create the new WebPhone instance
var phone = new WebPhone();

//Configure event listeners
phone
    .on('sipRegistered', onSipRegistered)
    .on('sipUnRegistered', onSipUnRegistered)
    .on('sipRegistrationFailed', onSipRegistrationFailed)
    .on('newLine', onNewLine)
    .on('callStarted', onCallStarted)
    .on('callEnded', onCallEnded)
    .on('callFailed', onCallEnded)
    .on('userMediaRequest', onUserMediaRequest)
    .on('userMediaAfterRequest', onUserMediaAfterRequest)
    .on('userMediaRequestDenied', onUserMediaRequestDenied);

//Set connection parameters
phone.setConfig({
    'uri': 'sip:5554443322@sip.server.com',
    'ws_servers': ['wss://some.ws.sip.server.com:443'],
    'password': 'mypass',
    'authorization_user': '9999999'
});
```

Then start:

```javascript
phone.start();
```


That's it. Now we need to handle the phone events:

Handle web-phone registration:

```javascript
//Event handlers
function onSipRegistered() {
    //Show phone controls
}

function onSipUnRegistered() {
    //Hide phone controls
}

function onSipRegistrationFailed() {
    //Hide phone controls
}
```

Handle new line:

```javascript
function onNewLine(line) {
    var lineId = line.getId();
    var direction = line.getDirection();
    var contact = line.getContact();

    if (direction === 'incoming') {
        handleIncomingCall();
    } else {
        handleOutgoingCall();
    }

    function handleOutgoingCall() {
         // If more than one line are active,
         // we can hold all lines except the new one
        if (phone.getAllLines().length > 1) {
            if(configrm('Hold other lines?')) {
                phone.holdAll([lineId]);
            }
        }
    }

    function handleIncomingCall() {
        // On incoming call we have to ask user for accept
        // or decline the call
        if(configrm('Incoming call from: ' + contact.number + '. Accept?')) {
            phone.answer(lineId);
        } else {
            phone.hangUp(lineId);
        }
    }

}
```


---

## API

### WebPhone Events

#### callEnded
On end of call:

```javascript
phone.on('callEnded', function(line) {
	//Handle call ending
});
```

#### callFailed
On call failed

```javascript
phone.on('callFailed', function(line) {
	//Handle call failing
});
```

#### callStarted
On Call started

```javascript
phone.on('callStarted', function(line) {
	//Handle call starting
});
```

#### holdLine
On hold or un-hold line

```javascript
phone.on('holdLine', function(line) {
	var onHold = line.isOnHold();
});
```

#### muteLine
On mute or un-mute line

```javascript
phone.on('muteLine', function(line) {
	var muted = line.isMuted();
});
```

#### newLine
On new line coming

```javascript
phone.on('newLine', function(line) {
	var direction = line.getDirection();
	if (direction === 'incoming') {
		handleIncomingCall();
	} else {
		handleOutgoingCall();
	}
})
```

#### newMessage
On new message

```javascript
phone.on('mewMessage', function() {
});
```

#### recordLine
On record start/stop

```javascript
phone.on('recordLine', function(line) {
	var onRecord = line.isOnRecord();
});
```

#### sipConnected
On web-socket connection established

```javascript
phone.on('sipConnected', function(line) {
	console.log('SIP connected');
});
```

#### sipDisconnected
On web-socket connection lost

```javascript
phone.on('sipDisconnected', function(line) {
	console.log('SIP disconnected');
});
```

#### sipRegistered
On SIP session opened

```javascript
phone.on('sipRegistered', function(line) {
	//Activate user interface
});
```

#### sipRegistrationFailed
On SIP registration error

```javascript
phone.on('sipRegistrationFailed', function(line) {
	//Deactivate user interface
});
```

#### sipUnRegistered
On SIP session closed

```javascript
phone.on('sipUnRegistered', function(line) {
	//Deactivate user interface
});
```

#### userMediaAfterRequest
On user allowed access to microphone

```javascript
phone.on('userMediaAfterRequest', function(line) {
	document.getElementById('mediaRequestDescription').style.display = 'none';
	//Activate user interface
});
```

#### userMediaRequest
On browser request for access to user microphone

```javascript
phone.on('userMediaRequest', function(line) {
	document.getElementById('mediaRequestDescription').style.display = 'block';
});
```

#### userMediaRequestDenied
On user denied access to microphone

```javascript
phone.on('userMediaAfterRequest', function(line) {
	document.getElementById('mediaRequestDescription').style.display = 'none';
	//Deactivate user interface
});
```

---

### WebPhone Interface

#### answer
```javascript
phone.answer(lineId)
```

Answer to incoming call.

- lineId - ID of line

#### call
```javascript
phone.call(number)
```
Make a new call

- number - Phone number. For example: "6505554433"


#### getAllLines
```javascript
phone.getAllLines()
```
Returns array of all active lines

#### getLine
```javascript
phone.getLine(lineId)
```
Returns the line by id

- lineId - ID of line

#### hangUp
```javascript
phone.hangUp(lineId)
```
Cancel the call

- lineId - ID of line


#### hangUpAll
```javascript
phone.hangUp(exceptList)
```
Cancel all the calls, except lines with IDs in excepted list

- exceptList - array of line IDs

#### hold
```javascript
phone.hold(lineId, force)
```
Toggle holding the line

- lineId - ID of line
- force (optional) - If true, line will not be un-holded if it currently on hold

#### holdAll
```javascript
phone.holdAll(exceptList)
```
Hold all the lines, except lines with IDs in excepted list

- exceptList - array of line IDs

#### mute
```javascript
phone.mute(lineId, force)
```
Toggle muting the line

- lineId - ID of line
- force (optional) - If true, line will not be un-muted if it currently muted

#### muteAll
```javascript
phone.muteAll(exceptList)
```
Mute all the lines, except lines with IDs in excepted list

- exceptList - array of line IDs

#### on
```javascript
phone.on(eventName, callback)
```
Subscribe to the web-phone event. Allows to add several listeners.

- eventName - name of event (see WebPhone Events)

#### preEvent
```javascript
phone.preEvent(callback)
```
Add hook on before event emitting.
The good point to manipulate on event data before it will be sent to listeners.

For example:

```javascript
phone.preEvent(function(name, data) {
	cons.display('Event: ' + name);
	return data;
});
```

#### record
```javascript
phone.record(lineId)
```
Start/stop recording the line

- lineId - ID of line

#### sendDTMF
```javascript
phone.record(lineId, code)
```
Send DTMF signal to line

- lineId - ID of line
- code - DTMF code: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, *, #

#### setConfig
```javascript
phone.setConfig(cfg)
```
Set phone config

For example:

```javascript
phone.setConfig({
	'uri': 'sip:5554443322@sip.server.com',
	'ws_servers': ['wss://some.ws.sip.server.com:443'],
	'password': 'mypass',
	'authorization_user': '9999999'
});
```

#### start
```javascript
phone.start()
```
Start web-phone

#### stop
```javascript
phone.stop()
```
Stop web-phone

---

### PhoneLine Interface

#### getCallDuration
```javascript
line.getCallDuration()
```
Returns call duration in milliseconds

#### getContact
```javascript
line.getContact()
```
Returns line contact:

```javascript
{
	name: 'Contact name',
	number: 5554443322
}
```

#### getDirection
```javascript
line.getDirection()
```
Returns call direction `"incoming"` or `"outgoing"`

#### getId
```javascript
line.getId()
```
Returns the line ID

#### getSession
```javascript
line.getSession()
```
Returns line session

#### isMuted
```javascript
line.isMuted()
```
Returns is the line is currently muted

#### isOnCall
```javascript
line.isOnCall()
```
Returns is the line is currently on active call

#### isOnHold
```javascript
line.isOnHold()
```
Returns is the line is currently on hold

#### isOnRecord
```javascript
line.isOnRecord()
```
Returns is the line is currently on record

---

## Development


### Demo App

#### How to start:

```sh
git clone https://github.com/ringcentral/web-phone.git
cd web-phone
npm install
npm start
```

Open in browser: `http://localhost:8000`

#### Demo app structure

Demo application is based on ReactJS framework.

```
/src
  /demo
    /jsx
      /phone
          dashboard.jsx
          dialpad.jsx
          line.jsx
          phone.jsx
          sidebar.jsx
       app.jsx
      console.jsx
      loginForm.jsx
    index.html
    style.css
```
