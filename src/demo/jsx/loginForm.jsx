var LoginForm = React.createClass({
    login: function (e) {
        e.preventDefault();
        this.props.onSubmit({
            'userName': this.refs.userName.getDOMNode().value,
            'extension': this.refs.extension.getDOMNode().value,
            'password': this.refs.password.getDOMNode().value,
            'appKey': this.refs.appKey.getDOMNode().value,
            'appSecret': this.refs.appSecret.getDOMNode().value,
            'server': this.refs.server.getDOMNode().value
        });
    },
    render: function () {
        return (
            <div className="panel panel-default login-form">
                <div className="panel-body">
                    <form className="form-horizontal">
                        <div className="form-group">
                            <label className="col-sm-2 control-label">User Name:</label>
                            <div className="col-sm-6">
                                <input type="text" ref="userName" className="form-control" defaultValue={this.props.opts.userName} />
                            </div>
                            <label className="col-sm-2 control-label">Extension:</label>
                            <div className="col-sm-2">
                                <input type="text" ref="extension" className="form-control" defaultValue={this.props.opts.extension} />
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-sm-2 control-label">Password:</label>
                            <div className="col-sm-10">
                                <input type="password" ref="password" className="form-control" defaultValue={this.props.opts.password} />
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-sm-2 control-label">App key:</label>
                            <div className="col-sm-10">
                                <input type="text" ref="appKey" className="form-control" defaultValue={this.props.opts.appKey} />
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-sm-2 control-label">App secret:</label>
                            <div className="col-sm-10">
                                <input type="text" ref="appSecret" className="form-control" defaultValue={this.props.opts.appSecret} />
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-sm-2 control-label">Server:</label>
                            <div className="col-sm-10">
                                <input type="text" ref="server" className="form-control" defaultValue={this.props.opts.server} />
                            </div>
                        </div>

                        <div className="form-group">
                            <label className="col-sm-2 control-label"></label>
                            <div className="col-sm-10">
                                <a href="https://developers.ringcentral.com/" target="_blank">Take the APP Key and APP Secret</a>
                            </div>
                        </div>

                        <div className="form-group">
                            <label className="col-sm-2 control-label"></label>
                            <div className="col-sm-10">
                                <a href="" className="btn btn-primary" onClick={this.login}>
                                    <i className="fa fa-sign-in"></i> Login
                                </a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        );
    }
});

