//var config = {
//    'authId': localStorage.authId || '',
//    'digest': localStorage.digest || '',
//    'outboundProxy': localStorage.outboundProxy || 'wss://webphone-sip.ringcentral.com:8083',
//    'proxy': localStorage.proxy || 'sip.ringcentral.com',
//    'userName': localStorage.userName || ''
//};

var config = {
    userName: localStorage.userName || '',
    extension: localStorage.extension || '',
    password: localStorage.password || '',
    appKey: localStorage.appKey || '',
    appSecret: localStorage.appSecret || '',
    server: localStorage.server || 'https://platform.devtest.ringcentral.com'
};

var phone = new WebPhone();

var Application = React.createClass({
    getInitialState: function () {
        return {
            regOptions: config,
            loggedIn: false,
            currentLine: undefined
        };
    },
    componentDidMount: function () {

        var self = this;
        var cons = this.refs['console'];

        cons.display('Ready!');

        phone.preEvent(function(name, data) {
            cons.display('Event: ' + name);
            return data;
        });

        phone
            .on('sipRegistered', onSipRegistered)
            .on('sipUnRegistered', onSipUnRegistered)
            .on('sipRegistrationFailed', onSipRegistrationFailed)
            .on('newLine', onNewLine)
            .on('callStarted', onCallStarted)
            .on('callEnded', onCallEnded)
            .on('callFailed', onCallEnded)
            .on('userMediaRequest', onUserMediaRequest)
            .on('userMediaAfterRequest', onUserMediaAfterRequest)
            .on('userMediaRequestDenied', onUserMediaRequestDenied);

        function onSipRegistered() {
            self.setState({
                loggedIn: true
            });
        }

        function onSipUnRegistered() {
            self.setState({
                loggedIn: false
            });
        }

        function onSipRegistrationFailed() {
            self.setState({
                loggedIn: false
            });
        }

        function onNewLine(line) {
            var dir = line.getDirection();
            cons.display(dir + ' call');
            self.setState({
                currentLine: line
            });
            if (dir === 'incoming') {
                handleIncomingCall();
            } else {
                handleOutgoingCall();
            }
        }

        function onUserMediaRequest() {
            self.refs["userMediaReqDiv"].getDOMNode().style.display = 'block';
        }

        function onUserMediaAfterRequest() {
            self.refs["userMediaReqDiv"].getDOMNode().style.display = 'none';
        }

        function onUserMediaRequestDenied() {
            self.refs["userMediaReqDiv"].getDOMNode().style.display = 'none';
        }

        function onCallStarted(line) {
            self.refs['dashBoard'].addLine(line);
        }

        function onCallEnded(line) {
            self.declineIncomingCall();
            self.refs['dashBoard'].removeLine(line);
            self.setState({
                currentLine: undefined
            });
        }

        function handleIncomingCall() {
            var d = self.refs['incomingDialog'].getDOMNode();
            $(d).modal({
                keyboard: false,
                backdrop: 'static'
            });
        }

        function handleOutgoingCall(line) {
            if (phone.getAllLines().length > 1) {
                self.askForHoldOtherLines(line.getId());
            }
        }

    },
    login: function (data) {
        var cons = this.refs['console'];
        cons.display('Login...');

        var rcsdk = new RCSDK({
            server: data.server,
            appKey: data.appKey,
            appSecret: data.appSecret
        });

        var platform = rcsdk.getPlatform();

        platform
            .authorize({
                username: data.userName,
                extension: data.extension,
                password: data.password
            })
            .then(getWebPhoneCreds)
            .catch(function (e) {
                cons.display('PLATFORM ERROR: ' + (e.message || 'Server cannot authorize user'));
            });

        localStorage.userName = data.userName;
        localStorage.extension = data.extension;
        localStorage.password = data.password;
        localStorage.appKey = data.appKey;
        localStorage.appSecret = data.appSecret;
        localStorage.server = data.server;

        function getWebPhoneCreds(ajax) {
            platform.apiCall({
                url: '/client-info/sip-provision',
                method: 'POST',
                post: {
                    "sipInfo": [{"transport": "WSS"}]
                }
            }).then(function(ajax){
                var data = JSON.parse(ajax.response)
                registerWebPhone(data.sipInfo[0]);
            }).catch(function(e){
                cons.display('PLATFORM ERROR: ' + e.message);
            });
        }

        function registerWebPhone(data) {
            phone.setConfig({
                'uri': 'sip:' + data.username + '@' + data.domain,
                'ws_servers': [data.transport.toLowerCase() + '://' + data.outboundProxy],
                'password': data.password,
                'authorization_user': data.authorizationId
            });
            phone.start();
        }
    },
    logout: function () {
        phone.stop();
    },
    onSelectLine: function (line) {
        this.setState({
            currentLine: line
        });
    },
    onCreateNewLine: function() {
        this.setState({
            currentLine: undefined
        });
    },
    call: function (number) {
        phone.call(number);
    },
    acceptIncomingCall: function() {
        var d = this.refs['incomingDialog'].getDOMNode();
        $(d).modal('hide');
        var line = this.state.currentLine;
        if(line) {
            phone.answer(line.getId())
        }
    },
    declineIncomingCall: function() {
        var d = this.refs['incomingDialog'].getDOMNode();
        $(d).modal('hide');
        this.hangUp();
    },
    sendDTMF: function(val) {
        if (this.state.currentLine) {
            phone.sendDTMF(this.state.currentLine.getId(), val);
        }
    },
    hangUp: function () {
        if (this.state.currentLine) {
            phone.hangUp(this.state.currentLine.getId());
        }
    },
    hangUpAll: function () {
        phone.hangUpAll();
    },
    hold: function () {
        var line = this.state.currentLine;
        if (line) {
            var lines = phone.getAllLines();
            if(line.isOnHold() && lines.length > 1) {
                this.askForHoldOtherLines(line.getId());
            }
            phone.hold(line.getId());
        }
    },
    mute: function() {
        var line = this.state.currentLine;
        if (line) {
            phone.mute(line.getId());
        }
    },
    record: function() {
        var line = this.state.currentLine;
        if (line) {
            phone.record(line.getId());
        }
    },
    muteAll: function() {
        phone.muteAll();
        this.refs['dashBoard'].forceUpdate();
    },
    askForHoldOtherLines: function(id) {
        this.refs['dashBoard'].refs['phone'].askForHoldOtherLines(function(res) {
            if(res) {
                phone.holdAll([id]);
            }
        });
    },
    holdAll: function () {
        phone.holdAll();
        this.refs['dashBoard'].forceUpdate();
    },
    render: function () {
        var panel;
        if (this.state.loggedIn) {
            panel = <DashBoard
                        ref="dashBoard"
                        currentLine={this.state.currentLine}
                        onLogout={this.logout}
                        onSelectLine={this.onSelectLine}
                        onCreateNewLine={this.onCreateNewLine}
                        onHold={this.hold}
                        onRecord={this.record}
                        onHoldAll={this.holdAll}
                        onMute={this.mute}
                        onMuteAll={this.muteAll}
                        onHangUp={this.hangUp}
                        onHangUpAll={this.hangUpAll}
                        onCall={this.call}
                        onSendDTMF={this.sendDTMF}
            />;
        } else {
            panel = <LoginForm ref="loginForm" opts={this.state.regOptions} onSubmit={this.login} />;
        }
        return (
            <div>
                <div ref="userMediaReqDiv" style={{display: 'none'}}>
                    <div className="screen-cover"></div>
                    <div className="screen-cover-text"><i className="fa fa-microphone"></i><br />Allow access to user media!</div>
                </div>
                <div ref="incomingDialog" className="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div className="modal-dialog modal-sm">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">INCOMING CALL</h4>
                            </div>
                            <div className="modal-body">
                                Name: <b>{this.state.currentLine ? this.state.currentLine.getContact()['name'] : ''}</b>
                                <br />
                                Number: <b>{this.state.currentLine ? this.state.currentLine.getContact()['number'] : ''}</b>
                                <hr />
                                <button type="button" className="btn btn-wide btn-success" onClick={this.acceptIncomingCall}>Accept</button>
                                <button type="button" className="btn btn-wide btn-danger" onClick={this.declineIncomingCall}>Decline</button>
                            </div>
                        </div>
                    </div>
                </div>
				{panel}
                <Console ref="console" />
            </div>
        );
    }
});


React.render(<Application />, document.getElementById('content'));
