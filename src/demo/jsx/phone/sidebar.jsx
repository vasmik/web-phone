var SideBar = React.createClass({
    logout: function (e) {
        e.preventDefault();
        this.props.onLogout();
    },
    selectLine: function (line) {
        if (typeof this.props['onSelectLine'] == 'function') {
            this.props['onSelectLine'](line);
        }
    },
    createNewLine: function () {
        if (typeof this.props['onCreateNewLine'] == 'function') {
            this.props['onCreateNewLine']();
        }
    },
    holdAll: function () {
        if (typeof this.props['onHoldAll'] == 'function') {
            this.props['onHoldAll']();
        }
    },
    muteAll: function () {
        if (typeof this.props['onMuteAll'] == 'function') {
            this.props['onMuteAll']();
        }
    },
    hangUpAll: function () {
        if (typeof this.props['onHangUpAll'] == 'function') {
            this.props['onHangUpAll']();
        }
    },
    render: function () {
        var self = this;
        var lineList = this.props.lines.map(function (line) {
            var isActive = self.props.currentLine && self.props.currentLine.getId() == line.getId();
            return (
                <Line line={line} onClick={self.selectLine} active={isActive} />
            )
        });

        return (
            <div>
                <div style={{float: 'left', marginBottom: '10px'}}>
                    <button disabled={this.props.currentLine == undefined} className="btn btn-xs btn-success" onClick={this.createNewLine}>
                        <i className="fa fa-plus"></i>&nbsp;New call
                    </button>
                    &nbsp;
                    <button disabled={this.props.lines.length == 0} className="btn btn-xs btn-warning" onClick={this.holdAll}>
                        <i className="fa fa-pause"></i>&nbsp;Hold All
                    </button>
                    &nbsp;
                    <button disabled={this.props.lines.length == 0} className="btn btn-xs btn-info" onClick={this.muteAll}>
                        <i className="fa fa-microphone-slash"></i>&nbsp;Mute All
                    </button>
                    &nbsp;
                    <button disabled={this.props.lines.length == 0} className="btn btn-xs btn-danger" onClick={this.hangUpAll}>
                        <i className="fa fa-close"></i>&nbsp;Hang Up All
                    </button>
                </div>

                <div style={{textAlign: 'right', marginBottom: '10px'}}>
                    <a href="" onClick={this.logout} className="btn btn-xs btn-default">
                        Logout&nbsp;<i className="fa fa-sign-in"></i>
                    </a>
                </div>

                <div className="row linesList">
                {lineList}
                </div>
            </div>
        );
    }

});