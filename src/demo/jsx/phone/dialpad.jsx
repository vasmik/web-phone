var DPButton = React.createClass({
    handleClick: function (e) {
        e.preventDefault();
        this.props.onClick(this.props.val);
    },
    render: function () {
        return (
            <a href="" onClick={this.handleClick} className="btn btn-primary">{this.props.val}
                <br />
                <small>{this.props.letters || '\u00A0' }</small>
            </a>
        );
    }
});


var Dialpad = React.createClass({
    pressButton: function (val) {
        this.props.onClick(val);
    },
    render: function () {
        return (
            <div className="dialpad">
                <div className="btn-group btn-group-justified">
                    <DPButton val="1" onClick={this.pressButton} />
                    <DPButton val="2" letters="ABC" onClick={this.pressButton} />
                    <DPButton val="3" letters="DEF"  onClick={this.pressButton}/>
                </div>
                <div className="btn-group btn-group-justified">
                    <DPButton val="4" letters="GHI" onClick={this.pressButton} />
                    <DPButton val="5" letters="JKL" onClick={this.pressButton} />
                    <DPButton val="6" letters="MNO" onClick={this.pressButton} />
                </div>
                <div className="btn-group btn-group-justified">
                    <DPButton val="7" letters="PQRS" onClick={this.pressButton} />
                    <DPButton val="8" letters="TUV" onClick={this.pressButton} />
                    <DPButton val="9" letters="WXYZ" onClick={this.pressButton} />
                </div>
                <div className="btn-group btn-group-justified">
                    <DPButton val="*" onClick={this.pressButton} />
                    <DPButton val="0" letters="+" onClick={this.pressButton} />
                    <DPButton val="#" onClick={this.pressButton} />
                </div>
            </div>
        );
    }
});