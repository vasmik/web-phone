var Line = React.createClass({
    getInitialState: function () {
        return {
            timer: undefined,
            duration: ''
        };
    },
    onClick: function () {
        if (typeof this.props['onClick'] === 'function') {
            this.props['onClick'](this.props.line);
        }
    },
    componentDidMount: function() {
        var self = this;

        showDuration();

        var timer = setInterval(showDuration, 1000);

        self.setState({
            timer: timer
        });

        function showDuration() {
            var dur = Math.ceil(self.props.line.getCallDuration() / 1000);
            var sec = dur % 60;
            var min = Math.floor(dur / 60);
            var hours = Math.floor(dur / 3600);

            self.setState({
                duration: f(hours, 2) + ':' + f(min, 2) + ':' + f(sec, 2)
            });

            function f(val, d) {
                return '000000'.substr(0, d - (val + '').length) + val;
            }
        }
    },
    componentWillUnmount: function () {
        clearInterval(this.state.timer);
    },
    render: function () {
        var line = this.props.line;
        var contact = line.getContact();
        var dirClass = (line.getDirection() === 'incoming' ? ' fa-arrow-circle-down' : ' fa-arrow-circle-up');
        var className = (this.props.active ? " active" : " inactive") + dirClass;
        return (
            <div className="col-xs-6">
                <div className="thumbnail line" onClick={this.onClick}>
                    <i className={"fa lamp" + className}></i>
                    <div>
                        <div>Name: {contact.name ? contact.name : 'None'}</div>
                        <div>Number: {contact.number}</div>
                    </div>
                    <div>
                        <small>
                            <span className="label label-default">{this.state.duration}</span>
                            &nbsp;
                            <span className="label label-warning" style={{display: this.props.line.isOnHold() ? 'inline' : 'none'}}><i className="fa fa-pause"></i></span>
                            &nbsp;
                            <span className="label label-info" style={{display: this.props.line.isMuted() ? 'inline' : 'none'}}><i className="fa fa-microphone-slash"></i></span>
                            &nbsp;
                            <span className="label label-danger" style={{display: this.props.line.isOnRecord() ? 'inline' : 'none'}}><i className="fa fa-circle"></i></span>
                        </small>
                    </div>
                </div>
            </div>
        );
    }
});