var DashBoard = React.createClass({
    getInitialState: function () {
        return {
            isOnCall: false,
            lines: []
        }
    },
    addLine: function (line) {
        this.state.lines.push(line);
        this.setState({
            lines: this.state.lines
        });
    },
    removeLine: function (line) {
        var lineId = line.getId();
        this.setState({
            lines: this.state.lines.filter(function (item) {
                return item.getId() != lineId;
            })
        });
    },
    render: function () {
        return (
            <div className="phone-container">
                <div className="phone">
                    <Phone
                        ref="phone"
                        currentLine={this.props.currentLine}
                        onCall={this.props.onCall}
                        onHold={this.props.onHold}
                        onRecord={this.props.onRecord}
                        onMute={this.props.onMute}
                        onHangUp={this.props.onHangUp}
                        onSendDTMF={this.props.onSendDTMF}
                    />
                </div>
                <div className="phone-side">
                    <div className="panel">
                        <div className="panel-body">
                            <SideBar ref="sideBar"
                                currentLine={this.props.currentLine}
                                lines={this.state.lines}
                                onSelectLine={this.props.onSelectLine}
                                onCreateNewLine={this.props.onCreateNewLine}
                                onHoldAll={this.props.onHoldAll}
                                onMuteAll={this.props.onMuteAll}
                                onHangUpAll={this.props.onHangUpAll}
                                onLogout={this.props.onLogout} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

