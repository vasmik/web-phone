var Phone = React.createClass({
    getInitialState: function () {
        return {
            lineStatus: 'New Line',
            timer: undefined,
            timerLine: undefined,
            onHold: false,
            muted: false,
            onRecord: false,
            holdDialogCallBack: undefined
        };
    },
    handleNumberChange: function () {
        var num = this.refs['phoneNumber'].getDOMNode().value.replace(/[^\d]/g, '');
        this.refs['phoneNumber'].getDOMNode().value = num;
        this.setState({
            number: num
        });
    },
    pressDialpadButton: function (val) {
        var isOnCall = this.props.currentLine !== undefined && this.props.currentLine.isOnCall();
        if(isOnCall) {
            this.props.onSendDTMF(val);
        } else {
            this.refs['phoneNumber'].getDOMNode().value += val;
            this.handleNumberChange();
        }
    },
    call: function (e) {
        e.preventDefault();
        if (this.state.number !== '' && typeof this.props.onCall === 'function') {
            this.props.onCall(this.state.number);
        }
    },
    hold: function(e) {
        e.preventDefault();
        this.props.onHold();
        this.setState({
            onHold: !this.state.onHold
        });
    },
    record: function(e) {
        e.preventDefault();
        this.props.onRecord();
        this.setState({
            onRecord: !this.state.onRecord
        });
    },
    mute: function(e) {
        e.preventDefault();
        this.props.onMute();
        this.setState({
            muted: !this.state.muted
        });
    },
    answerHoldDialog: function (val) {
        this.state.holdDialogCallBack(val);
        this.setState({
            holdDialogCallBack: undefined
        });
    },
    askForHoldOtherLines: function(cb) {
        this.setState({
            holdDialogCallBack: cb
        });
    },
    hangUp: function (e) {
        e.preventDefault();
        this.props.onHangUp();
    },
    componentWillUnmount: function () {
        if (this.state.timer !== undefined) {
            clearInterval(this.state.timer);
        }
    },
    componentWillReceiveProps: function(props) {
        var self = this;
        this.refs['phoneNumber'].getDOMNode().value = props.currentLine !== undefined ? props.currentLine.getContact()['number'] : '';
        this.handleNumberChange();
        var timer = this.state.timer;
        var newState = {};
        if (props.currentLine !== undefined) {
            if (props.currentLine.isOnCall() && (timer == undefined || this.state.timerLine != props.currentLine.getId() )) {
                if (timer) {
                    clearInterval(timer);
                }
                showDuration();
                timer = setInterval(showDuration, 1000);
                newState['timer'] = timer;
                newState['timerLine'] = props.currentLine.getId();
            } else if (this.state.timerLine != props.currentLine.getId()) {
                newState['lineStatus'] = 'Calling...';
            }
            newState['onHold'] = props.currentLine.isOnHold();
            newState['muted'] = props.currentLine.isMuted();
            newState['onRecord'] = props.currentLine.isOnRecord();
        } else {
            if(timer !== undefined) {
                clearInterval(timer);
                timer = undefined;
            }
            newState['timer'] = timer;
            newState['lineStatus'] = 'New Line';
        }

        this.setState(newState);

        function showDuration() {
            var dur = Math.ceil(props.currentLine.getCallDuration() / 1000);
            var sec = dur % 60;
            var min = Math.floor(dur / 60);
            var hours = Math.floor(dur / 3600);

            self.setState({
                timer: timer,
                lineStatus: 'Duration: ' + f(hours, 2) + ':' + f(min, 2) + ':' + f(sec, 2)
            });

            function f(val, d) {
                var sval = val + '';
                return '000000'.substr(0, d - sval.length) + val;
            }
        }

    },
    render: function () {
        var isOnCall = this.props.currentLine !== undefined && this.props.currentLine.isOnCall();
        return (
            <div className="dialpad panel">
                <div className="panel-body" style={{position: 'relative'}}>
                    <input className="form-control" disabled={isOnCall} type="text" defaultValue="" ref="phoneNumber" onChange={this.handleNumberChange}/>

                    <div style={{margin: '5px 0'}}>{this.state.lineStatus}&nbsp;</div>

                    <Dialpad onClick={this.pressDialpadButton} />

                    <div className="btn-group btn-group-justified" style={{'margin-top': '10px'}}>
                        <a disabled={!isOnCall} className={"btn btn-xs btn-default" + (this.state.onHold ? " active": "")} onClick={this.hold}><i className="fa fa-pause"></i>&nbsp;Hold</a>
                        <a disabled={!isOnCall} className={"btn btn-xs btn-default" + (this.state.muted ? " active": "")} onClick={this.mute}><i className="fa fa-microphone-slash"></i>&nbsp;Mute</a>
                        <a disabled={!isOnCall} className={"btn btn-xs btn-default" + (this.state.onRecord ? " active": "")} onClick={this.record}><i className="fa fa-circle"></i>&nbsp;Record</a>
                    </div>

                    <br />

                    <div className="btn-group btn-group-justified" style={{'display': this.props.currentLine ? 'none' : 'block'}}>
                        <a href="" onClick={this.call} className={"btn btn-success" + (this.state.number ? "" : " disabled")}>
                            <i className="fa fa-phone"></i> Call
                        </a>
                    </div>

                    <div className="btn-group btn-group-justified" style={{'display': this.props.currentLine ? 'block' : 'none'}}>
                        <a href="" onClick={this.hangUp} className="btn btn-danger">
                            <i className="fa fa-stop"></i> Hang Up
                        </a>
                    </div>

                    <div style={{display: (typeof this.state.holdDialogCallBack == 'function') ? 'block': 'none'}} className="hold-dialog">
                        <div className="panel panel-primary">
                            <div className="panel-body">
                                <div style={{textAlign: 'center'}}>
                                    <div><b>Hold other lines?</b></div><br />
                                    <button className="btn btn-default" onClick={this.answerHoldDialog.bind(this, true)}>Yes</button>
                                    &nbsp;
                                    <button className="btn btn-default" onClick={this.answerHoldDialog.bind(this, false)}>No</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }


});
