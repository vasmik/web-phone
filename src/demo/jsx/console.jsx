var Console = React.createClass({
    display: function (text) {
        var t = new Date();
        var s = this.refs.screen.getDOMNode();
        s.innerHTML += (f(t.getHours(), 2) + ':' + f(t.getMinutes(), 2) + ':' + f(t.getSeconds(), 2) + '.' + f(t.getMilliseconds(), 3) + '> ' + text + '<br />');
        s.scrollTop = s.scrollHeight;

        function f(val, d) {
            var sval = val + '';
            return '000000'.substr(0, d - sval.length) + val;
        }
    },
    clear: function (e) {
        e.preventDefault();
        this.refs.screen.getDOMNode().innerHTML = '';
    },
    render: function () {
        return (
            <div className="console panel panel-default">
                <div className="panel-body">
                    <div className="screen-border">
                        <a href="" className="btn btn-xs btn-default" onClick={this.clear}>
                            <i className="glyphicon glyphicon-remove" />
                        </a>
                        <div className="screen" ref="screen"></div>
                    </div>
                </div>
            </div>
        );
    }
});

