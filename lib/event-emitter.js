/*--------------------------------------------------------------------------------------------------------------------*/

var EventEmitter = function() {
    this.handlers = {};
    this.preEventHandler = undefined;
};

/*--------------------------------------------------------------------------------------------------------------------*/

EventEmitter.prototype.emit = function (name, data) {
    if(typeof this.preEventHandler === 'function') {
        data = this.preEventHandler(name, data);
    }
    if (name in this.handlers) {
        var list = this.handlers[name];
        for (var i = 0; i < list.length; i++) {
            setTimeout(executeListener(list[i]), 0);
        }
    }
    function executeListener(listener) {
        return function () {
            listener(data);
        };
    }
};

/*--------------------------------------------------------------------------------------------------------------------*/

EventEmitter.prototype.on = function (name, listener) {
    this.handlers[name] = this.handlers[name] || [];
    var list = this.handlers[name];
    list.push(listener);
};

/*--------------------------------------------------------------------------------------------------------------------*/

EventEmitter.prototype.preEvent = function(callback) {
    this.preEventHandler = callback;
};

/*--------------------------------------------------------------------------------------------------------------------*/

EventEmitter.prototype.Event = {
    callEnded: 'callEnded',
    callFailed: 'callFailed',
    callStarted: 'callStarted',
    //message: 'message',
    newMessage: 'newMessage',
    muteLine: 'muteLine',
    holdLine: 'holdLine',
    recordLine: 'recordLine',
    sipConnected: 'sipConnected',
    sipDisconnected: 'sipDisconnected',
    sipRegistered: 'sipRegistered',
    sipRegistrationFailed: 'sipRegistrationFailed',
    sipUnRegistered: 'sipUnRegistered',
    userMediaAfterRequest: 'userMediaAfterRequest',
    userMediaRequest: 'userMediaRequest',
    userMediaRequestDenied: 'userMediaRequestDenied',
    newLine: 'newLine'
};

/*--------------------------------------------------------------------------------------------------------------------*/

module.exports = EventEmitter;

