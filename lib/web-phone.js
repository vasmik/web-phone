var EventEmitter = require('./event-emitter');
var PhoneLine = require('./phone-line');

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Constructor
 * @param {Object} cfg
 * @constructor
 */
var WebPhone = function (cfg) {
    var self = this;
    this.lines = [];
    this.config = cfg || {};
    this.DTMFSEnders = {};
    this.ua = undefined;
    this.eventEmitter = new EventEmitter();
    this.on(this.eventEmitter.Event.callEnded, function (ln) {
        removeLine.call(self, ln.getId());
    });
    this.on(this.eventEmitter.Event.callFailed, function (ln) {
        removeLine.call(self, ln.getId());
    });


    function userMediaHack(implementation) {
        var originalGetUserMedia = implementation;
        return function (constraints, callback, errback) {
            var timer = setTimeout(function () {
                self.eventEmitter.emit(self.eventEmitter.Event.userMediaRequest, constraints);
            }, 50);
            originalGetUserMedia.call(this, constraints,
                function () {
                    clearTimeout(timer);
                    self.eventEmitter.emit(self.eventEmitter.Event.userMediaAfterRequest);
                    return typeof callback == "function" ? callback.apply(this, arguments) : undefined;
                },
                function (err) {
                    clearTimeout(timer);
                    self.eventEmitter.emit(self.eventEmitter.Event.userMediaRequestDenied);
                    return typeof errback == "function" ? errback.apply(this, arguments) : undefined;
                }
            );
        };
    }

    if (window.navigator.webkitGetUserMedia) {
        window.navigator.webkitGetUserMedia = userMediaHack(window.navigator.webkitGetUserMedia);
        if (window.JsSIP !== undefined) {
            window.JsSIP.WebRTC.getUserMedia = window.navigator.webkitGetUserMedia.bind(navigator);
        }
    } else if (window.navigator.mozGetUserMedia) {
        window.navigator.mozGetUserMedia = userMediaHack(window.navigator.mozGetUserMedia);
        if (window.JsSIP !== undefined) {
            window.JsSIP.WebRTC.getUserMedia = window.navigator.mozGetUserMedia.bind(navigator);
        }
    } else if (window.navigator.getUserMedia) {
        window.navigator.getUserMedia = userMediaHack(window.navigator.getUserMedia);
        if (window.JsSIP !== undefined) {
            window.JsSIP.WebRTC.getUserMedia = window.navigator.getUserMedia.bind(navigator);
        }
    }
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Add listener by event name
 * @param {String} name
 * @param {Function} listener
 * @returns {WebPhone}
 */
WebPhone.prototype.on = function (name, listener) {
    this.eventEmitter.on(name, listener);
    return this;
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Add hook on before event emitting.
 * The good point to manipulate on event data before it will be sent to listeners.
 * @param {Function} handler
 */
WebPhone.prototype.preEvent = function (handler) {
    this.eventEmitter.preEvent(handler);
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Set phone config
 * @param {Object} cfg
 */
WebPhone.prototype.setConfig = function (cfg) {
    this.config = cfg || {};
    this.config.stun_servers = [];
    this.config.turn_servers = [];
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Start phone
 */
WebPhone.prototype.start = function () {
    var self = this;

    this.ua = new JsSIP.UA(this.config);

    this.ua.on('connected', function (e) {
        self.eventEmitter.emit(self.eventEmitter.Event.sipConnected, e);
    });
    this.ua.on('disconnected', function (e) {
        self.eventEmitter.emit(self.eventEmitter.Event.sipDisconnected, e);
    });
    this.ua.on('registered', function (e) {
        self.eventEmitter.emit(self.eventEmitter.Event.sipRegistered, e);
    });
    this.ua.on('unregistered', function (e) {
        self.eventEmitter.emit(self.eventEmitter.Event.sipUnRegistered, e);
    });
    this.ua.on('registrationFailed', function (e) {
        self.eventEmitter.emit(self.eventEmitter.Event.sipRegistrationFailed, e);
    });
    this.ua.on('newRTCSession', function (e) {
        var line = new PhoneLine({
            session: e.data.session,
            instanceId: self.config.authorization_user,
            eventEmitter: self.eventEmitter
        });
        self.lines.push(line);
        self.eventEmitter.emit(self.eventEmitter.Event.newLine, line);
    });
    this.ua.on('newMessage', function (e) {
    });

    this.ua.start();
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Stop phone
 */
WebPhone.prototype.stop = function () {
    if (this.ua) {
        this.ua.stop();
        this.ua = undefined;
    }
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Call to phone number
 * @param {String} number
 */
WebPhone.prototype.call = function (number) {

    var self = this;
    var localPlayer;
    var remotePlayer;

    var options = {
        'mediaConstraints': {'audio': true, 'video': false},
        'eventHandlers': {
            'started': function (e) {
                addPlayers(e);
            },
            'ended': function () {
                removePlayers();
            },
            'failed': function () {
                removePlayers();
            }
        },
        'RTCConstraints': {
            'optional': [
                {'DtlsSrtpKeyAgreement': 'true'}
            ]
        }
    };

    this.ua.call(number, options);

    function addPlayers(e) {
        localPlayer = document.createElement('video');
        localPlayer.autoplay = 'true';
        localPlayer.style.display = 'none';
        document.body.appendChild(localPlayer);
        remotePlayer = document.createElement('video');
        remotePlayer.autoplay = 'true';
        remotePlayer.style.display = 'none';
        document.body.appendChild(remotePlayer);

        var sid = e.sender.id;
        var session = self.ua.sessions[sid];
        if (session.getLocalStreams().length > 0) {
            localPlayer.src = window.URL.createObjectURL(session.getLocalStreams()[0]);
            localPlayer.volume = 0;
        }
        if (session.getRemoteStreams().length > 0) {
            remotePlayer.src = window.URL.createObjectURL(session.getRemoteStreams()[0]);
        }
    }

    function removePlayers() {
        if (localPlayer && remotePlayer) {
            localPlayer.remove();
            remotePlayer.remove();
            localPlayer = undefined;
            remotePlayer = undefined;
        }
    }

};


/*--------------------------------------------------------------------------------------------------------------------*/


/**
 * Answer to incoming call
 * @param {Number} lineId
 */
WebPhone.prototype.answer = function (lineId) {
    var line = this.getLine(lineId);
    if(line) {
        var session = line.getSession();
        session.answer({mediaConstraints: { audio: true, video: false }});
    }
};


/*--------------------------------------------------------------------------------------------------------------------*/

function removeLine(lineId) {
    var s = this.DTMFSEnders[lineId];
    if(s) {
        clearTimeout(s.timer);
        delete this.DTMFSEnders[lineId];
    }
    this.lines = this.lines.filter(function (i) {
        return i.getId() != lineId;
    });
}


/*--------------------------------------------------------------------------------------------------------------------*/

function applyForLines(lines, exceptList, fn) {
    exceptList = exceptList || [];
    lines.map(function (item) {
        var itemId = item.getId();
        var skip = false;
        for (var i = 0; i < exceptList.length; i++) {
            if (exceptList[i] === itemId) {
                skip = true;
                break;
            }
        }
        if (!skip) {
            fn(itemId);
        }
    });
}


/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Hang Up the line
 * @param {Number} lineId
 */
WebPhone.prototype.hangUp = function (lineId) {
    var line = this.getLine(lineId);
    if(line) {
        var session = line.getSession();
        if (session) {
            try {
                session.terminate({
                    status_code: 486
                });
            } catch (e) {
                console.error(e);
            }
        }
    }
    removeLine.call(this, lineId);
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Hang Up all the lines, except the lines from specified list of IDs
 * @param {Array} [exceptList]
 */
WebPhone.prototype.hangUpAll = function (exceptList) {
    var self = this;
    applyForLines(this.lines, exceptList, function (id) {
        self.hangUp(id)
    });
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Send DTMF signal to line
 * @param {Number} lineId
 * @param {String} code
 */
WebPhone.prototype.sendDTMF = function (lineId, code) {
    var line = this.getLine(lineId);
    if(line) {
        this.DTMFSEnders[lineId] = this.DTMFSEnders[lineId] || getDTMFSender.call(this, line);
        this.DTMFSEnders[lineId].push(code);

    } else {
        delete this.DTMFSEnders[lineId];
    }
};


/*--------------------------------------------------------------------------------------------------------------------*/

function getDTMFSender(line) {
    return {
        line: line,
        progress: false,
        timer: undefined,
        next: function () {
            if(this.progress) {
                return;
            }
            var self = this;
            var q = this.line.DTMFQueue;
            if(q.length > 0) {
                this.progress = true;
                var value = q.shift();
                var duration = 300;

                var peer = this.line.getSession().rtcMediaHandler.peerConnection;
                var stream = this.line.getSession().getLocalStreams()[0];
                var dtmfSender = peer.createDTMFSender(stream.getAudioTracks()[0]);
                if (dtmfSender !== undefined && dtmfSender.canInsertDTMF) {
                    dtmfSender.insertDTMF(value, duration);
                }

                this.timer = setTimeout(function() {
                    self.progress = false;
                    self.next();
                }, duration);
            }
        },
        push: function (code) {
            this.line.DTMFQueue.push(code);
            this.next();
        }
    }
}

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Start/stop recording the line
 * @param {Number} lineId
 */
WebPhone.prototype.record = function (lineId) {
    var line = this.getLine(lineId);
    if (line && line.isOnCall()) {
        line.onRecord = !line.onRecord;
        this.eventEmitter.emit(this.eventEmitter.Event.recordLine, line);
        var body = '<Msg><Hdr SID="" Req="" From="" To="" Cmd="31"/><Bdy Cln="' + line.instanceId + '"/></Msg>';
        var options = {
            contentType: "x-rc/agent",
            session: line.getSession(),
            inDialog: true
        };
        this.ua.sendSIP(JsSIP.C.INFO, line.getContact()['number'], body, options);
    }
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Mute/un-mute the line
 * @param {Number} lineId
 * @param {Boolean} [force]
 */
WebPhone.prototype.mute = function (lineId, force) {
    var toggle = force !== true;
    var line = this.getLine(lineId);
    if (line) {
        if (line.muted && !toggle) {
            return;
        }
        line.muted = !line.muted;
        this.eventEmitter.emit(this.eventEmitter.Event.muteLine, line);
        try {
            var tracks = line.getSession().getLocalStreams()[0].getAudioTracks();
            for (var i = 0; i < tracks.length; i++) {
                tracks[i].enabled = !line.muted;
            }
        } catch (e) {
            console.error(e);
        }
    }
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Mute all the lines except the lines from specified list of IDs
 * @param {Array} [exceptList]
 */
WebPhone.prototype.muteAll = function (exceptList) {
    var self = this;
    applyForLines(this.lines, exceptList, function (id) {
        self.mute(id, true);
    });
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Hold/un-hold the line
 * @param {Number} lineId
 * @param {Boolean} [force]
 */
WebPhone.prototype.hold = function (lineId, force) {
    var toggle = force !== true;
    var line = this.getLine(lineId);
    if (line && line.isOnCall()) {
        if (line.onHold && !toggle) {
            return;
        }
        line.onHold = !line.onHold;
        this.eventEmitter.emit(this.eventEmitter.Event.holdLine, line);
        var session = line.getSession();
        var body = session.rtcMediaHandler.peerConnection['localDescription'].sdp;
        if (line.onHold) {
            body = body.replace(/c=IN IP4 \d+\.\d+.\d+.\d+/, "c=IN IP4 0.0.0.0");
            body = body.replace(/a=sendrecv/, "a=sendonly");
        }
        var options = {
            contentType: "application/sdp",
            session: session,
            inDialog: true,
            extraHeaders: [
                "Contact: " + session.contact,
                "P-rcid:" + line.instanceId
            ],
            eventHandlers: {
                'succeeded': function () {
                    session.sendACK();
                }
            }
        };
        this.ua.sendSIP(JsSIP.C.INVITE, line.getContact()['number'], body, options);
    }
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Hold all the lines except the lines from specified list of IDs
 * @param [exceptList]
 */
WebPhone.prototype.holdAll = function (exceptList) {
    var self = this;
    applyForLines(this.lines, exceptList, function (id) {
        self.hold(id, true);
    });
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Get line by ID
 * @param {Number} id
 * @returns {PhoneLine}
 */
WebPhone.prototype.getLine = function (id) {
    var line;
    for (var i = 0; i < this.lines.length; i++) {
        if (this.lines[i].getId() == id) {
            line = this.lines[i];
            break;
        }
    }
    return line;
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Get all the lines
 * @returns {PhoneLine[]}
 */
WebPhone.prototype.getAllLines = function () {
    return this.lines;
};

/*--------------------------------------------------------------------------------------------------------------------*/

module.exports = WebPhone;