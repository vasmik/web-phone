/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Constructor
 * @param {Object} options
 * @constructor
 */
var PhoneLine = function (options) {
    this.session = options.session;
    this.eventEmitter = options.eventEmitter;
    this.instanceId = options.instanceId;
    this.onCall = false;
    this.onHold = false;
    this.onRecord = false;
    this.muted = false;
    this.DTMFQueue = [];
    this.timeCallStarted = undefined;
    this.direction = '';
    this.contact = {
        name: '',
        phone: ''
    };

    var self = this;

    this.session.on('started', function () {
        self.onCall = true;
        self.timeCallStarted = new Date();
        self.eventEmitter.emit(self.eventEmitter.Event.callStarted, self);
    });

    this.session.on('ended', function () {
        self.onCall = false;
        self.timeCallStarted = undefined;
        self.eventEmitter.emit(self.eventEmitter.Event.callEnded, self);
    });

    this.session.on('failed', function () {
        self.onCall = false;
        self.timeCallStarted = undefined;
        self.eventEmitter.emit(self.eventEmitter.Event.callFailed, self);
    });

    if (this.session.direction === 'incoming') {
        this.direction = 'incoming';
        this.contact.name = this.session.request.from.display_name;
        this.contact.number = this.session.request.from.uri.user;
    } else {
        this.direction = 'outgoing';
        this.contact.name = this.session.request.to.display_name;
        this.contact.number = this.session.request.to.uri.user;
    }
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Get line ID
 * @returns {Number}
 */
PhoneLine.prototype.getId = function () {
    return this.session.id;
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Is line on call
 * @returns {Boolean}
 */
PhoneLine.prototype.isOnCall = function () {
    return !!this.onCall;
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Is line on hold
 * @returns {Boolean}
 */
PhoneLine.prototype.isOnHold = function() {
    return !!this.onHold;
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Is line muted
 * @returns {Boolean}
 */
PhoneLine.prototype.isMuted = function() {
    return !!this.muted;
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Is line on record
 * @returns {Boolean}
 */
PhoneLine.prototype.isOnRecord = function() {
    return !!this.onRecord;
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Returns call duration in milliseconds
 * @returns {Number}
 */
PhoneLine.prototype.getCallDuration = function () {
    if (this.timeCallStarted) {
        return (new Date()).getTime() - this.timeCallStarted.getTime();
    } else {
        return 0;
    }
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Returns call direction "incoming" or "outgoing"
 * @returns {String}
 */
PhoneLine.prototype.getDirection = function () {
    return this.direction;
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Returns line session
 * @returns {Object}
 */
PhoneLine.prototype.getSession = function () {
    return this.session;
};

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * Returns line contact
 * @returns {{name: String, phone: String}}
 */
PhoneLine.prototype.getContact = function () {
    return this.contact;
};

/*--------------------------------------------------------------------------------------------------------------------*/

module.exports = PhoneLine;