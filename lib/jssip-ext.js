(function () {

    var SIPSender;

    SIPSender = function (ua) {
        this.ua = ua;
        this.direction = null;
        this.local_identity = null;
        this.remote_identity = null;
        this.data = {};
    };

    SIPSender.prototype = new JsSIP.EventEmitter();

    SIPSender.prototype.send = function (type, target, body, options) {
        var request_sender, event, contentType, eventHandlers, extraHeaders,
            events = [
                'succeeded',
                'failed'
            ],
            invalidTarget = false;

        if (type === undefined || target === undefined || body === undefined) {
            throw new TypeError('Not enough arguments');
        }

        var allowedTypes = [
            JsSIP.C.ACK,
            JsSIP.C.BYE,
            JsSIP.C.CANCEL,
            JsSIP.C.INFO,
            JsSIP.C.INVITE,
            JsSIP.C.MESSAGE,
            JsSIP.C.NOTIFY,
            JsSIP.C.OPTIONS,
            JsSIP.C.REGISTER,
            JsSIP.C.UPDATE,
            JsSIP.C.SUBSCRIBE
        ];

        var allowedType = false;
        for (var t = 0; t < allowedTypes.length; t++) {
            if (allowedTypes[t] === type) {
                allowedType = true;
                break;
            }
        }

        if (!allowedType) {
            throw new TypeError('Not allowed type: ' + type);
        }

        this.initEvents(events);

        // Get call options
        options = options || {};
        extraHeaders = options.extraHeaders || [];
        eventHandlers = options.eventHandlers || {};
        contentType = options.contentType || 'text/plain';

        // Set event handlers
        for (event in eventHandlers) {
            this.on(event, eventHandlers[event]);
        }

        // Check target validity
        try {
            target = JsSIP.Utils.normalizeURI(target, this.ua.configuration.hostport_params);
        } catch (e) {
            target = JsSIP.URI.parse(JsSIP.C.INVALID_TARGET_URI);
            invalidTarget = true;
        }

        // SIPSender parameter initialization
        this.direction = 'outgoing';
        this.local_identity = this.ua.configuration.uri;
        this.remote_identity = target;

        this.closed = false;
        this.ua.applicants[this] = this;

        extraHeaders.push('Content-Type: ' + contentType);


        if (options.session !== undefined && options.inDialog === true) {
            this.request = options.session.dialog.createRequest(type, extraHeaders);
        } else {
            this.request = new JsSIP.OutgoingRequest(type, target, this.ua, null, extraHeaders);
        }

        if (body) {
            this.request.body = body;
        }

        request_sender = new JsSIP.RequestSender(this, this.ua);

        if (invalidTarget) {
            this.emit('failed', this, {
                originator: 'local',
                cause: JsSIP.C.causes.INVALID_TARGET
            });
        } else {
            request_sender.send();
        }
    };

    SIPSender.prototype.receiveResponse = function (response) {
        var cause;

        if (this.closed) {
            return;
        }
        switch (true) {
            case /^1[0-9]{2}$/.test(response.status_code):
                // Ignore provisional responses.
                break;

            case /^2[0-9]{2}$/.test(response.status_code):
                delete this.ua.applicants[this];
                this.emit('succeeded', this, {
                    originator: 'remote',
                    response: response
                });
                break;

            default:
                delete this.ua.applicants[this];
                cause = JsSIP.Utils.sipErrorCause(response.status_code);
                this.emit('failed', this, {
                    originator: 'remote',
                    response: response,
                    cause: cause
                });
                break;
        }
    };


    SIPSender.prototype.onRequestTimeout = function () {
        if (this.closed) {
            return;
        }
        this.emit('failed', this, {
            originator: 'system',
            cause: JsSIP.C.causes.REQUEST_TIMEOUT
        });
    };

    SIPSender.prototype.onTransportError = function () {
        if (this.closed) {
            return;
        }
        this.emit('failed', this, {
            originator: 'system',
            cause: JsSIP.C.causes.CONNECTION_ERROR
        });
    };

    SIPSender.prototype.close = function () {
        this.closed = true;
        delete this.ua.applicants[this];
    };

    JsSIP.SIPSender = SIPSender;

    JsSIP.UA.prototype.sendSIP = function (type, target, body, options) {
        var sipSender;
        sipSender = new JsSIP.SIPSender(this);
        sipSender.send(type, target, body, options);
    };

// Patch to accept voicemail messages "x-rc/agent"
    JsSIP.Message.prototype.init_incoming = function (request) {
        var transaction,
            contentType = request.getHeader('content-type');

        this.direction = 'incoming';
        this.request = request;
        this.local_identity = request.to.uri;
        this.remote_identity = request.from.uri;

        if (contentType && (contentType.match(/^text\/plain(\s*;\s*.+)*$/i) || contentType.match(/^text\/html(\s*;\s*.+)*$/i) || contentType.match(/^x-rc\/agent(\s*;\s*.+)*$/i))) {
            this.ua.emit('newMessage', this.ua, {
                originator: 'remote',
                message: this,
                request: request
            });

            transaction = this.ua.transactions.nist[request.via_branch];

            if (transaction && (transaction.state === JsSIP.Transactions.C.STATUS_TRYING || transaction.state === JsSIP.Transactions.C.STATUS_PROCEEDING)) {
                request.reply(200);
            }
        } else {
            request.reply(415, null, ['Accept: text/plain, text/html']);
        }
    };

})();
